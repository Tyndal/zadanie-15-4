//Zadanie 1

const a = "Hello";
const b = "World";

const sayHello = `${a} ${b}`;

console.log(sayHello);

//Zadanie 2

const multiply = (a = 1, b = 1) => a * b;
console.log(multiply(4, 5));
console.log(multiply(4));

//Zadanie 3

const average = (...args) => {
  let b = 0;
  args.forEach(arg => {
    b += arg;
  });
  return `Średnia wynosi: ${b / args.length}`;
};

console.log(average(1, 2, 3, 4));
console.log(average(0, 2, 6, 8, 19));

//Zadanie 4

const grades = [1, 5, 5, 5, 4, 3, 3, 2, 1];

average(...grades);

console.log(average(...grades));

//Zadanie 5

const array = [1, 4, "Iwona", false, "Nowak"];

const [, , firstName, , lastName] = array;

console.log(`First name is ${firstName}`);
console.log(`Last name is ${lastName}`);
