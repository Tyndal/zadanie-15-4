"use strict";

//Zadanie 1

var a = "Hello";
var b = "World";

var sayHello = a + " " + b;

console.log(sayHello);

//Zadanie 2

var multiply = function multiply() {
  var a = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
  var b = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  return a * b;
};
console.log(multiply(4, 5));
console.log(multiply(4));

//Zadanie 3

var average = function average() {
  for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  var b = 0;
  args.forEach(function (arg) {
    b += arg;
  });
  return "\u015Arednia wynosi: " + b / args.length;
};

console.log(average(1, 2, 3, 4));
console.log(average(0, 2, 6, 8, 19));

//Zadanie 4

var grades = [1, 5, 5, 5, 4, 3, 3, 2, 1];

average.apply(undefined, grades);

console.log(average.apply(undefined, grades));

//Zadanie 5

var array = [1, 4, "Iwona", false, "Nowak"];

var firstName = array[2],
    lastName = array[4];


console.log("First name is " + firstName);
console.log("Last name is " + lastName);
